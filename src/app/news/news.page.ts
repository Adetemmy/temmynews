import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {
  news: any= [{
    id: 1,
    image: '', 
    title: 'crime',
    description:'Paedophile Nigeria pastor rapes 6 girls in UK. His wife Aides & Abet Rape'
  },  {
    id: 2,
    image: '',
    title: 'politics',
    description: "President Buhari reacts to Hope Uzodinma's Supreme court victory"
  },  {
    id: 3,
   image: '',
   title: 'politics',
   description: 'Governor wike asks traditional rulers to standup with their staff of office'
 },  {
   id: 4,
   image: '',
   title: 'politics',
   description: 'Supreme court judgement: the destiny of Imo State has been taken away'
 },  {
   id: 5,
   image: '',
   title: 'crime',
   description: '21 years old most deadly female cartel boss killed in military shoutout in mexico'
 },  {
   id: 6,
   image: '',
   title: 'politics',
   description: "we are expecting Buhari' decision on IPPIS by ASUU president"
 },  {
   id: 7,
   image: '',
   title: 'politics',
   description: 'Ibrahim Babaginda says Amotekun not viable, advises south-west governors'
 },  {
   id:8,
   image: '',
   title: 'technology',
   description: 'here is the latest iphone12'
 }
 
 ]
 public description
 public title
 public image
  constructor(public route: ActivatedRoute) { }

  ngOnInit() {
    let ev = this.route.snapshot.paramMap.get('ev')
    console.log(ev)
    let r = [...this.news]
    let content = r.filter(data => data.id == ev)
    this.description = content[0].description
    this.title = content[0].title
    this.image = content[0].image
    // console.log(content[0])
  }

}
